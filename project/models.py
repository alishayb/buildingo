from django.db import connection

# Create your models here.

class Project:
    def dictfetchall(cursor):
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    @classmethod
    def getList(self,user_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM PROJECT WHERE owner_id = %s ORDER BY status DESC;",[user_id])
            list_project = cursor.fetchall()
            return list_project

    @classmethod
    def getStatusPIC(self,user_id,project_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT s.name FROM PIC p, STATUS_PIC s WHERE p.status = s.code AND p.id_professional = %s AND p.id_project = %s",[user_id,project_id])
            status = cursor.fetchall()
            return status

    @classmethod
    def acceptProject(self,user_id,project_id):
        with connection.cursor() as cursor:
            cursor.execute("UPDATE PIC SET status ='ACC' WHERE  id_professional = %s AND id_project = %s",[user_id,project_id])
            return True
    
    @classmethod
    def rejectProject(self,user_id,project_id):
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM PIC WHERE id_professional = %s AND id_project = %s",[user_id,project_id])
            return True

    @classmethod
    def getListProjectInCharge(self,user_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM PROJECT p, PIC c WHERE p.id = c.id_project AND c.id_professional = %s; ",[user_id])
            list_project = cursor.fetchall()
            return list_project

    def verifypic(self, project, pic):
        project_type = project[0][1]
        architect = False
        contractor = False
        if project_type == "Design" : 
            for elem in pic :
                if elem[1] == "Architect":
                    architect = True
        elif project_type == "Construct":
            for elem in pic :
                if elem[1] == "Contractor":
                    contractor = True
        else :
            for elem in pic :
               if elem[1] == "Architect":
                   architect = True
               if elem[1] == "Contractor":
                    contractor = True
        pic_status = (architect,contractor)
        return pic_status

    @classmethod
    def getProject(self,project_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT p.title, p.type, s.name, b.name, p.estimated_time, p.city, p.province, p.building_type, p.budget_range, p.length, p.width, p.building_style, p.needs, p.wants FROM PROJECT p, PROJECT_OWNER o, BUILDINGO_USER b,STATUS_PROJECT s  WHERE p.owner_id = o.id AND o.id = b.id AND p.status = s.code AND p.id = %s;",[project_id])
            project = cursor.fetchall()
            cursor.execute("SELECT b.name, r.professional_type, t.name FROM PIC p, PROFESSIONAL r, BUILDINGO_USER b, STATUS_PIC t WHERE t.code =p.status AND b.id= r.id AND p.id_professional = r.id AND id_project = %s;",[project_id])
            pic = cursor.fetchall()
            cursor.execute("SELECT * FROM IMAGE_PROJECT WHERE id = %s",[project_id])
            pic_status = self.verifypic(self,project,pic)
            list_image = cursor.fetchall()
            cursor.execute("SELECT * FROM FILE WHERE id = %s",[project_id])
            list_file = cursor.fetchall()
            project_container = (project,list_image,list_file,pic,pic_status)
            return project_container

    @classmethod
    def createForm(self):
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM BUILDING_TYPE")
            building_type = cursor.fetchall()
            cursor.execute("SELECT * FROM STYLE_PREFERENCES")
            building_style = cursor.fetchall()
            cursor.execute("SELECT * FROM PROVINCE")
            province = cursor.fetchall()
            createForm = {
                'building_type' : building_type,
                'building_style' : building_style,
                'province' : province
            }
            return createForm

    @classmethod
    def createProject(self,dictDetail):
        with connection.cursor() as cursor:
            tuple_query = [
                dictDetail['projectId'],dictDetail['projectOwner'],dictDetail['projectTitle'],
                dictDetail['projectType'],dictDetail['estimatedTime'],dictDetail['budgetRange'],dictDetail['width'],
                dictDetail['length'], dictDetail['city'],dictDetail['province'],dictDetail['needs'], dictDetail['wants'], 
                dictDetail['buildingType'],dictDetail['stylePreferences'],'OPN'
                ]
            cursor.execute("INSERT INTO PROJECT VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",tuple_query)
            return True


    @classmethod
    def createProjectImage(self,project_id,imageUrl):
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO IMAGE_PROJECT VALUES(%s,%s)",[project_id,imageUrl])
            return True
    

    @classmethod
    def editProject(self,dictDetail):
        with connection.cursor() as cursor:
            tuple_query = [
            dictDetail['projectTitle'],
            dictDetail['projectType'],dictDetail['estimatedTime'],dictDetail['budgetRange'],dictDetail['width'],
            dictDetail['length'], dictDetail['city'],dictDetail['province'],dictDetail['needs'], dictDetail['wants'], 
            dictDetail['buildingType'],dictDetail['stylePreferences'],dictDetail['projectId'],
            ]
            cursor.execute("UPDATE PROJECT SET title = %s, type = %s, estimated_time = %s, budget_range = %s, width = %s, length = %s, city = %s, province = %s, needs= %s, wants = %s ,building_type = %s, building_style = %s  WHERE id = %s ",tuple_query)
        return True

    @classmethod
    def checkFromImage(self,project_id,image_url):
        with connection.cursor() as cursor:
            cursor.execute("SELECT count(id) FROM IMAGE_PROJECT WHERE id = %s AND url = %s",[project_id,image_url])
            isExist = cursor.fetchall()[0][0]
            if isExist:
                return True
            else :
                return False

    @classmethod
    def checkFromFile(self,project_id,file_url):
        with connection.cursor() as cursor:
            cursor.execute("SELECT count(id) FROM FILE WHERE id = %s AND url = %s",[project_id,file_url])
            isExist = cursor.fetchall()[0][0]
            if isExist:
                return True
            else :
                return False

    @classmethod
    def createProjectFile(self,project_id,imageUrl):
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO FILE VALUES(%s,%s)",[project_id,imageUrl])
            return True 

    @classmethod
    def deleteProject(self,project_id):
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM PROJECT WHERE id=%s",[project_id])
            return True
    
    @classmethod
    def cancelProject(self,project_id,reason):
        with connection.cursor() as cursor:
            cursor.execute("UPDATE PROJECT SET status = 'CRQ' WHERE id = %s",[project_id])
            cursor.execute("INSERT INTO CANCELLATION VALUES(%s,%s,'CRQ')",[project_id,reason])
            return True
    
    @classmethod
    def getListImageUrl(self,project_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT url FROM IMAGE_PROJECT WHERE id=%s",[project_id])
            listUrl = cursor.fetchall()
            return listUrl

    @classmethod
    def getListFileUrl(self,project_id):
        with connection.cursor() as cursor:
            cursor.execute("SELECT url FROM FILE WHERE id=%s",[project_id])
            listUrl = cursor.fetchall()
            return listUrl

    @classmethod
    def getNewProjectId(self):
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM PROJECT ORDER BY id DESC limit 1")
            last_project_id = cursor.fetchall()
            if(len(last_project_id)>0):
                last_project_id = last_project_id[0][0]
                new_id = int(last_project_id[2:]) +1
                new_project_id = "PJ"+ str(new_id)
            else : 
                new_project_id ="PJ1"
            return new_project_id

    @classmethod
    def projectReady(self,type,pic):
        status_architect = False
        status_contractor = False
        for elem in pic :
            if elem[2] == "ACCEPTED":
                if elem[1] == "Architect":
                    status_architect = True
                else :
                    status_contractor = True
        if type == "Design" :
            cummulative_status = status_architect
        elif type == "Construct":
            cummulative_status = status_contractor
        else :
            cummulative_status = status_architect and status_contractor
        return cummulative_status

    @classmethod
    def startProject(self,project_id):
        with connection.cursor() as cursor:
            cursor.execute("UPDATE PROJECT SET status = 'ONG' WHERE id = %s",[project_id])
            return True
