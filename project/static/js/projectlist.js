$(".status-item").click(function() {
  console.log("clicked")
  var idActiveStatus = $(".status-active").attr("id")
  var idActiveList = $(".list-active").attr("id")

  var idTargetStatus =$(this).attr("id")
  var idTargetList = "list-"+idTargetStatus

  var activeStatus = $("#"+idActiveStatus)
  activeStatus.removeClass("status-active")
  var targetStatus = $("#"+idTargetStatus)
  targetStatus.addClass("status-active")

  var activeList = $("#"+idActiveList)
  activeList.css("display","none")
  activeList.removeClass("list-active")

  var targetList = $("#"+idTargetList)
  targetList.css("display","block")
  targetList.addClass("list-active")
});

$(document).ready(function() {
  $('#modalAlert').modal('show')
});


$('#modalDelete').on('show.bs.modal', function(e) {
  console.log("tes")
  var url_delete = $(e.relatedTarget).data('url-delete');
  console.log(url_delete)
  $("#deleteFinal").attr("href", url_delete);
});

$('#modalAccept').on('show.bs.modal', function(e) {
  console.log("tes")
  var url_accept = $(e.relatedTarget).data('url-accept');
  console.log(url_accept)
  $("#AcceptFinal").attr("href", url_accept);
});

$('#modalReject').on('show.bs.modal', function(e) {
  console.log("tes")
  var url_reject = $(e.relatedTarget).data('url-reject');
  console.log(url_reject)
  $("#rejectFinal").attr("href", url_reject);
});

$('#modalCancellation').on('show.bs.modal', function(e) {
  console.log("tes")
  var url_cancel = $(e.relatedTarget).data('url-cancel');
  console.log(url_cancel)
  $("#cancelFinal").attr("action", url_cancel);
});