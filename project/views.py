from django.http.response import Http404
from django.shortcuts import redirect, render
from django.db import connection
from django.http import request
from django.contrib import messages
from .models import *
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile, File
import os
from wsgiref.util import FileWrapper
import mimetypes
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import messages
import datetime

def project(request):
    role = request.session['role']
    user_id = request.session['id']
    if role == "Project Owner":
        list_project = Project.getList(user_id) 
    elif role == "Professional":
        list_project = Project.getListProjectInCharge(user_id)
    else :
        return redirect('verifikasi:listProfessional')
    context = {
            'role' : role,
            'list_project' : list_project
        }
    return render(request,'project/projectlist.html',context)

def create_project(request):
    role = request.session['role']
    user_id = request.session['id']

    if request.method == "POST" :
        print(request.POST.getlist('imageArr[]'))
        new_projectId = Project.getNewProjectId()
        
        dictDetail = {
            'projectId': new_projectId,'projectOwner': user_id,
            'projectTitle' : request.POST['projectTitle'], 'projectType' : request.POST['projectType'], 'estimatedTime' : request.POST['estimatedTime'],
            'budgetRange' : request.POST['budgetRange'], 'length' : request.POST['length'], 'width' : request.POST['width'],
            'province' : request.POST['province'], 'city' : request.POST['city'],
            'buildingType' : request.POST['buildingType'], 'stylePreferences' : request.POST['stylePreferences'],
            'needs' : request.POST['needs'], 'wants': request.POST['wants']
            }
        
        if(Project.createProject(dictDetail)):

            if 'imageinput' in request.FILES:
                images = request.FILES['imageinput']
                imageTitle = "img/{}-img1.png".format(new_projectId)
                if(Project.createProjectImage(new_projectId,imageTitle)):
                    path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
            if 'imageinput2' in request.FILES:
                images = request.FILES['imageinput2']
                imageTitle = "img/{}-img2.png".format(new_projectId)
                if(Project.createProjectImage(new_projectId,imageTitle)):
                    path2= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
            if 'imageinput3' in request.FILES:
                images = request.FILES['imageinput3']
                imageTitle = "img/{}-img3.png".format(new_projectId)
                if(Project.createProjectImage(new_projectId,imageTitle)):
                    path3= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))

            if 'additionalFile' in request.FILES:
                files = request.FILES['additionalFile']
                fileTitle ="file/{}-file1.pdf".format(new_projectId)
                if(Project.createProjectFile(new_projectId,fileTitle)):
                    path4 = default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))
            if 'additionalFile2' in request.FILES:
                files = request.FILES['additionalFile2']
                fileTitle ="file/{}-file2.pdf".format(new_projectId)
                if(Project.createProjectFile(new_projectId,fileTitle)):
                    path5 = default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))

        messages.success(request, "Project Successfully Created")
        return redirect('project:projectList')
    else :
        context = {
            'role' : role,
            'create_form' : Project.createForm()
        }
    return render(request,'project/createproject.html',context)

def edit_project(request,edit_id):
    role = request.session['role']
    user_id = request.session['id']
    if request.method == "POST" :
        new_projectId = edit_id

        dictDetail = {
            'projectId': new_projectId,'projectOwner': user_id,
            'projectTitle' : request.POST['projectTitle'], 'projectType' : request.POST['projectType'], 'estimatedTime' : request.POST['estimatedTime'],
            'budgetRange' : request.POST['budgetRange'], 'length' : request.POST['length'], 'width' : request.POST['width'],
            'province' : request.POST['province'], 'city' : request.POST['city'],
            'buildingType' : request.POST['buildingType'], 'stylePreferences' : request.POST['stylePreferences'],
            'needs' : request.POST['needs'], 'wants': request.POST['wants']
            }
        if(Project.editProject(dictDetail)):
            if 'imageinput' in request.FILES:
                images = request.FILES['imageinput']
                imageTitle = "img/{}-img1.png".format(new_projectId)
                print("ssss")
                if((Project.checkFromImage(new_projectId,imageTitle))):
                    default_storage.delete("project/static/"+imageTitle)
                    path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
                else:
                    if(Project.createProjectImage(new_projectId,imageTitle)):
                        path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
            if 'imageinput2' in request.FILES:
                images = request.FILES['imageinput2']
                imageTitle = "img/{}-img2.png".format(new_projectId)
                if((Project.checkFromImage(new_projectId,imageTitle))):
                    default_storage.delete("project/static/"+imageTitle)
                    path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
                else:
                    if(Project.createProjectImage(new_projectId,imageTitle)):
                        path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
            if 'imageinput3' in request.FILES:
                images = request.FILES['imageinput3']
                imageTitle = "img/{}-img3.png".format(new_projectId)
                print("here")
                if((Project.checkFromImage(new_projectId,imageTitle))):
                    print("here1")
                    default_storage.delete("project/static/"+imageTitle)
                    path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))
                else:
                    print("here2")
                    if(Project.createProjectImage(new_projectId,imageTitle)):
                        path= default_storage.save("project/static/"+imageTitle, ContentFile(images.read()))

            if 'additionalFile' in request.FILES:
                files = request.FILES['additionalFile']
                fileTitle ="file/{}-file1.pdf".format(new_projectId)
                if((Project.checkFromFile(new_projectId,fileTitle))):
                    default_storage.delete("project/static/"+fileTitle)
                    path= default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))
                else:
                    if(Project.createProjectFile(new_projectId,fileTitle)):
                        path= default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))
            if 'additionalFile2' in request.FILES:
                files = request.FILES['additionalFile2']
                fileTitle ="file/{}-file2.pdf".format(new_projectId)
                if((Project.checkFromFile(new_projectId,fileTitle))):
                    default_storage.delete("project/static/"+fileTitle)
                    path= default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))
                else:
                    if(Project.createProjectFile(new_projectId,fileTitle)):
                        path= default_storage.save("project/static/"+fileTitle, ContentFile(files.read()))

        messages.success(request, "Project Successfully Edited")
        return redirect('project:projectList')
    else :
        project_container = Project.getProject(edit_id)
        image_container = (project_container[1],len(project_container[1]))
        file_container = (project_container[2],len(project_container[2]))
        context = {
            'role' : role,
            'project':project_container[0],
            'image_list':image_container,
            'file_list':file_container,
            'project_id':edit_id,
            'create_form' : Project.createForm()
            }
        print(project_container[0][0][4])
    return render(request,'project/editproject.html',context)



def project_detail(request,detail_id):
    role = request.session['role']
    project_container = Project.getProject(detail_id)
    image_container = (project_container[1],len(project_container[1]))
    file_container = (project_container[2],len(project_container[2]))
    context = {
        'role' : role,
        'project':project_container[0],
        'image_list':image_container,
        'file_list':file_container,
        'project_id':detail_id,
        'need_architect':project_container[4][0],
        'need_contractor':project_container[4][1]
        }
    if role == "Professional":
        user_id = request.session['id']
        status_pic = Project.getStatusPIC(user_id,detail_id)
        context['status']= status_pic[0][0]

    for elem in project_container[3]:
        if elem[1] == 'Architect':
            context['architect_name'] = elem[0]
            context['architect_status'] = elem[2]
        else :
            context['contractor_name'] = elem[0]
            context['contractor_status'] = elem[2]
    
    project_ready = Project.projectReady(project_container[0][0][1],project_container[3])
    context['project_ready']=project_ready
    return render(request,'project/projectdetail.html',context)

def delete_project(request, delete_id):
    listImageUrl = Project.getListImageUrl(delete_id)
    listFileUrl = Project.getListFileUrl(delete_id)
    Project.deleteProject(delete_id)
    for elem in listImageUrl :
        default_storage.delete("project/static/"+elem[0])
    for elem in listFileUrl :
        default_storage.delete("project/static/"+elem[0])
    messages.success(request, "Project has been deleted")
    return redirect('project:projectList')

def cancellation_project(request, cancel_id):
    print(cancel_id)
    reason = request.POST['reason']
    Project.cancelProject(cancel_id,reason)
    messages.success(request, "Project Cancellation has been requested")
    return redirect('project:projectList')


def pdf_download(request,filename):
    with default_storage.open("project/static/"+filename,'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'inline ; filename=' +os.path.basename(filename)
            return response

def accept_project(request,project_id):
    user_id = request.session['id']
    Project.acceptProject(user_id,project_id)
    messages.success(request, "Project has been accepted")
    return redirect('project:projectList')

def reject_project(request,project_id):
    user_id = request.session['id']
    Project.rejectProject(user_id,project_id)
    messages.success(request, "Project has been rejected")
    return redirect('project:projectList')

def start_project(request,project_id):
    Project.startProject(project_id)
    messages.success(request, "Project has been started")
    return redirect('project:projectDetail',project_id)