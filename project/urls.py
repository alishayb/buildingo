from os import name
from django.urls import path
from . import views

app_name = 'project'

urlpatterns = [
    path('project', views.project, name="projectList"),
    path('createProject',views.create_project,name="createProject"),
    path('editproject<str:edit_id>',views.edit_project,name="editproject"),
    path('projectdetail/<str:detail_id>',views.project_detail,name="projectDetail"),
    path('deleteproject/<str:delete_id>',views.delete_project,name="deleteproject"),
    path('cancelproject/<str:cancel_id>',views.cancellation_project,name="cancelproject"),
    path('filedownload/<path:filename>',views.pdf_download, name="download"),
    path('acceptproject/<str:project_id>',views.accept_project, name="acceptproject"),
    path('rejectproject/<str:project_id>',views.reject_project, name="rejectproject"),
    path('startproject/<str:project_id>',views.start_project,name="startproject")
]