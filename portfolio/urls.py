from django.urls import path
from . import views

app_name = 'portfolio'

urlpatterns = [
    path('portfolio', views.portfolio, name="portfolio"),
    path('portfolio/<str:name>', views.portfolioOutside, name='portfolioOutside'),
    path('portfolio/<str:name>/<str:id>/', views.detailPortfolioOutside, name='detailPortfolioOutside'),
    path('create-portfolio', views.createPortfolio, name="createPortfolio"),
    path('detail-portfolio/<str:id>/', views.detailPortfolio, name="detailPortfolio"),
    path('edit-portfolio/<str:id>/', views.editPortfolio, name="editPortfolio"),
    path('delete-portfolio/<str:id>/', views.deletePortfolio, name="deletePortfolio"),
]