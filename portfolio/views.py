from django.shortcuts import render
from django.shortcuts import redirect
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.db import connection, DatabaseError
from django.db.utils import IntegrityError
from django.contrib import messages
import datetime

# Create your views here.

def dictFetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def portfolio(request):
    context = {}
    role = request.session['role']
    context['role'] = role
    if role =='Professional':
        professional_id = request.session['id']
        with connection.cursor() as cursor:
            query = "SELECT * FROM PORTFOLIO WHERE professional_id =" + "'" + professional_id  + "'"
            
            cursor.execute(query)
            context['list_portfolio'] = dictFetch(cursor)

        return render(request, 'portfolio.html', context)
    else:
        print("hello")

def portfolioOutside(request,name):
    context = {}
    context["role"] = request.session['role']
    with connection.cursor() as cursor:
        query = "SELECT id FROM BUILDINGO_USER WHERE name =" + "'" + name  + "'"
        cursor.execute(query)
        professional_id = dictFetch(cursor)[0]['id']

        query = "SELECT * FROM PORTFOLIO WHERE professional_id =" + "'" + professional_id  + "'"
            
        cursor.execute(query)
        context['list_portfolio'] = dictFetch(cursor)
        
        context['name'] = name

        return render(request, 'portfolio-outside.html', context)


def createPortfolio(request):
    context = {}
    role = request.session['role']
    context['role'] = role
    if role =='Professional':
        professional_id = request.session['id']
        with connection.cursor() as cursor:
            query = "SELECT * from PROVINCE"
            cursor.execute(query)
            context['list_province'] = dictFetch(cursor)

            query = "SELECT * from building_type"
            cursor.execute(query)
            context['list_building_type'] = dictFetch(cursor)

            query = "SELECT * from style_preferences"
            cursor.execute(query)
            context['list_building_style'] = dictFetch(cursor)

        if request.method == 'POST':
            try:
                name = request.POST["name"]
                description = request.POST["description"]
                province = request.POST["province"]
                city = request.POST["city"]
                buildingType = request.POST["buildingType"]
                buildingStyle =  request.POST["buildingStyle"]
                projectCost =  request.POST["projectCost"]
                projectYear =  request.POST["projectYear"]
                photo1 =  request.FILES["photo1"]

                now = datetime.datetime.now()
                if int(projectYear) > now.year:
                    context['error_message'] = 'Your project year is invalid.'
                    return render(request, "create-portfolio.html", context)

                if 'photo2' in request.FILES:
                    photo2 =  request.FILES["photo2"]
                else:
                    photo2= False
                
                if 'photo3' in request.FILES:
                    photo3 =  request.FILES["photo3"]
                else:
                    photo3= False

                if 'photo4' in request.FILES:
                    photo4 =  request.FILES["photo4"]
                else:
                    photo4= False

                with connection.cursor() as cursor:
                    query = " SELECT MAX(id) AS maxid from portfolio"
                    cursor.execute(query)
                    id = dictFetch(cursor)
                    new_id=""
                    print(len(id))
                    if id[0]['maxid'] == None:
                        new_id = 'PT1'
                    else:
                        id_number = int(id[0]['maxid'].split("PT")[1])+1
                        new_id = "PT" + str(id_number)

                    cursor.execute(f"INSERT INTO PORTFOLIO VALUES ('{new_id}','{name}','{professional_id}','{city}','{description}','{buildingStyle}','{buildingType}','{projectCost}','{projectYear}','{province}')")

                    path_photo1 = "portfolio/static/portfolio/images/" + new_id + "_img1.jpg"
                    path = default_storage.save(path_photo1, ContentFile(photo1.read()))
                    pathdb_photo1 = "/static/portfolio/images/"+ new_id + "_img1.jpg"
                    cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{new_id}','{pathdb_photo1}')")

                    if photo2 != False:
                        path_photo2 = "portfolio/static/portfolio/images/"+ new_id + "_img2.jpg"
                        path = default_storage.save(path_photo2, ContentFile(photo2.read()))
                        pathdb_photo2 = "/static/portfolio/images/"+ new_id + "_img2.jpg"
                        cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{new_id}','{pathdb_photo2}')")

                    if photo3 != False:
                        path_photo3 = "portfolio/static/portfolio/images/" + new_id + "_img3.jpg"
                        path = default_storage.save(path_photo3, ContentFile(photo3.read()))
                        pathdb_photo3 = "/static/portfolio/images/"+ new_id + "_img3.jpg"
                        cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{new_id}','{pathdb_photo3}')")

                    if photo4 != False:
                        path_photo4 = "portfolio/static/portfolio/images/" + new_id + "_img4.jpg"
                        path = default_storage.save(path_photo4, ContentFile(photo4.read()))
                        pathdb_photo4 = "/static/portfolio/images/"+ new_id + "_img4.jpg"
                        cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{new_id}','{pathdb_photo4}')")
                    
                    return redirect("/portfolio")

            except DatabaseError:
                context['error_message'] = 'Your input data is invalid. Please input valid data.'
                return render(request, "edit-portfolio.html", context)

        
        return render(request, 'create-portfolio.html', context)
    else:
        print("hello")

def detailPortfolio(request,id):
    context = {}
    role = request.session['role']
    context['role'] = role
    with connection.cursor() as cursor:

        query = "SELECT * FROM PORTFOLIO WHERE id =" + "'" + id  + "'"
        cursor.execute(query)
        context['portfolio'] = dictFetch(cursor)[0]

        query = "SELECT * FROM IMAGE_PORTFOLIO where portfolio_id ="  + "'" + id  + "'"
        cursor.execute(query)
        context['list_photo'] = dictFetch(cursor)

        context['main_photo'] = context['list_photo'][0]

    return render(request, 'detail-portfolio.html', context)

def detailPortfolioOutside(request,name,id):
    context = {}
    role = request.session['role']
    context['role'] = role
    context['name'] = name
    with connection.cursor() as cursor:

        query = "SELECT * FROM PORTFOLIO WHERE id =" + "'" + id  + "'"
        cursor.execute(query)
        context['portfolio'] = dictFetch(cursor)[0]

        query = "SELECT * FROM IMAGE_PORTFOLIO where portfolio_id ="  + "'" + id  + "'"
        cursor.execute(query)
        context['list_photo'] = dictFetch(cursor)

        context['main_photo'] = context['list_photo'][0]

    return render(request, 'detail-portfolio-outside.html', context)

def deletePortfolio(request,id):
    context = {}
    role = request.session['role']
    context['role'] = role
    if role =='Professional':
        professional_id = request.session['id']
        with connection.cursor() as cursor:

            print(id)
            query = "DELETE FROM IMAGE_PORTFOLIO WHERE portfolio_id =" + "'" + id  + "'"
            cursor.execute(query)

            path_photo1 = "portfolio/static/portfolio/images/"+ id + "_img1.jpg"
            default_storage.delete(path_photo1)

            path_photo2 = "portfolio/static/portfolio/images/"+ id + "_img2.jpg"
            default_storage.delete(path_photo2)

            path_photo3 = "portfolio/static/portfolio/images/"+ id + "_img3.jpg"
            default_storage.delete(path_photo3)

            path_photo4 = "portfolio/static/portfolio/images/"+ id + "_img4.jpg"
            default_storage.delete(path_photo4)

            query = "DELETE FROM PORTFOLIO WHERE id =" + "'" + id  + "'"
            cursor.execute(query)


        return redirect("/portfolio")
    else:
        print("hello")

def editPortfolio(request,id):
    context = {}
    role = request.session['role']
    context['role'] = role
    if role =='Professional':
        professional_id = request.session['id']
        with connection.cursor() as cursor:
            query = "SELECT * from PROVINCE"
            cursor.execute(query)
            context['list_province'] = dictFetch(cursor)

            query = "SELECT * from building_type"
            cursor.execute(query)
            context['list_building_type'] = dictFetch(cursor)

            query = "SELECT * from style_preferences"
            cursor.execute(query)
            context['list_building_style'] = dictFetch(cursor)

            query = "SELECT * from PORTFOLIO where id=" + "'" + id  + "'"
            cursor.execute(query)
            data_portfolio = dictFetch(cursor)
            professional_id = data_portfolio[0].get('professional_id')
            context['id'] = id
            context['name'] = data_portfolio[0].get('name')
            print(context['name'])
            context['description'] = data_portfolio[0].get('description')
            print(context['description'])
            context['province'] = data_portfolio[0].get('province')
            context['city'] = data_portfolio[0].get('city')
            context['buildingType'] = data_portfolio[0].get('building_type')
            context['buildingStyle'] = data_portfolio[0].get('building_style')
            context['projectCost'] = data_portfolio[0].get('project_cost')
            context['projectYear'] = data_portfolio[0].get('project_year')

        if request.method == 'POST':
            try:
                name = request.POST["name"]
                description = request.POST["description"]
                province = request.POST["province"]
                city = request.POST["city"]
                buildingType = request.POST["buildingType"]
                buildingStyle =  request.POST["buildingStyle"]
                projectCost =  request.POST["projectCost"]
                projectYear =  request.POST["projectYear"]


                if 'photo1' in request.FILES:
                    photo1 =  request.FILES["photo1"]
                else:
                    photo1= False

                if 'photo2' in request.FILES:
                    photo2 =  request.FILES["photo2"]
                else:
                    photo2= False
                
                if 'photo3' in request.FILES:
                    photo3 =  request.FILES["photo3"]
                else:
                    photo3= False

                if 'photo4' in request.FILES:
                    photo4 =  request.FILES["photo4"]
                else:
                    photo4= False
                
                now = datetime.datetime.now()
                if int(projectYear) > now.year:
                    context['error_message'] = 'Your project year is invalid.'
                    return render(request, "edit-portfolio.html", context)

                with connection.cursor() as cursor:
                    cursor.execute(f"UPDATE PORTFOLIO SET name='{name}', professional_id='{professional_id}',city='{city}',description='{description}',building_style='{buildingStyle}',building_type='{buildingType}',project_cost={projectCost},project_year={projectYear},province='{province}' where id='{id}'")
                    
                    if photo1 != False:
                        path_photo1 = "portfolio/static/portfolio/images/"+ id + "_img1.jpg"
                        default_storage.delete(path_photo1)
                        path = default_storage.save(path_photo1, ContentFile(photo1.read()))

                    cursor.execute(f"SELECT * FROM IMAGE_PORTFOLIO where url='/static/portfolio/images/{id}_img2.jpg'")
                    pathdb_photo2 = dictFetch(cursor)
                    if photo2!= False:
                        path_photo2 = "portfolio/static/portfolio/images/"+ id + "_img2.jpg"
                        if len(pathdb_photo2)!=0:
                            default_storage.delete(path_photo2)
                            default_storage.save(path_photo2, ContentFile(photo2.read()))
                        else:
                            default_storage.save(path_photo2, ContentFile(photo2.read()))
                            pathdb_photo2 = "/static/portfolio/images/"+ id + "_img2.jpg"
                            cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{id}','{pathdb_photo2}')")

                    cursor.execute(f"SELECT * FROM IMAGE_PORTFOLIO where url='/static/portfolio/images/{id}_img3.jpg'")
                    pathdb_photo3 = dictFetch(cursor)
                    if photo3!= False:
                        path_photo3 = "portfolio/static/portfolio/images/"+ id + "_img3.jpg"
                        if len(pathdb_photo3)!=0:
                            default_storage.delete(path_photo3)
                            default_storage.save(path_photo3, ContentFile(photo3.read()))
                        else:
                            default_storage.save(path_photo3, ContentFile(photo3.read()))
                            pathdb_photo3 = "/static/portfolio/images/"+ id + "_img3.jpg"
                            cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{id}','{pathdb_photo3}')")

                    cursor.execute(f"SELECT * FROM IMAGE_PORTFOLIO where url='/static/portfolio/images/{id}_img4.jpg'")
                    pathdb_photo4 = dictFetch(cursor)
                    if photo4!= False:
                        path_photo4 = "portfolio/static/portfolio/images/"+ id + "_img4.jpg"
                        if len(pathdb_photo4)!=0:
                            default_storage.delete(path_photo4)
                            default_storage.save(path_photo4, ContentFile(photo4.read()))
                        else:
                            default_storage.save(path_photo4, ContentFile(photo4.read()))
                            pathdb_photo4 = "/static/portfolio/images/"+ id + "_img4.jpg"
                            cursor.execute(f"INSERT INTO IMAGE_PORTFOLIO VALUES ('{id}','{pathdb_photo4}')")
                    
                    return redirect("/portfolio")

            except DatabaseError:
                context['error_message'] = 'Your input data is invalid. Please input valid data.'
                return render(request, "edit-portfolio.html", context)

        return render(request, 'edit-portfolio.html', context)
    else:
        print("hello")
