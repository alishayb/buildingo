from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.sessions.models import Session
from django.db import connection
from django.db.utils import InternalError, IntegrityError
from django.contrib import messages
import datetime

# Create your views here.
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def listProfessional(request):
    role = request.session['role']
    if role == 'Administrator':
        with connection.cursor() as cursor:
            cursor.execute("select * from professional, buildingo_user where professional.id = buildingo_user.id and professional.status = 'VIP'")
            professional = dictfetchall(cursor)
            cursor.execute("select * from professional, buildingo_user, professional_ver where professional.id = professional_ver.id and professional.id = buildingo_user.id and professional.status != 'UNV' and professional.status != 'VIP'")
            verified = dictfetchall(cursor)
            context = {
                'role': role,
                'professional':professional,
                'verified':verified,
            }
        return render(request, 'listProfessional.html', context)

def listProject(request):
    role = request.session['role']
    if role == 'Administrator':
        with connection.cursor() as cursor:
            cursor.execute("select * from cancellation, project where cancellation.project_id = project.id and cancellation.status != 'ACC'")
            project = dictfetchall(cursor)
            cursor.execute("select * from cancellation, project where cancellation.project_id = project.id and cancellation.status = 'ACC'")
            verified = dictfetchall(cursor)
            context = {
                'role': role,
                'project':project,
                'verified':verified,
            }
        return render(request, 'listProject.html', context)

def detailProject(request, id):
    role = request.session['role']
    if role == 'Administrator':
        with connection.cursor() as cursor:
            cursor.execute("select * from cancellation, project, buildingo_user where cancellation.project_id = project.id and buildingo_user.id = project.owner_id and cancellation.status = 'CRQ' and cancellation.project_id=%s",[id])
            row = dictfetchall(cursor)
            cursor.execute("select pic.id_project, buildingo_user.name, professional.professional_type, pic.status from pic, professional, buildingo_user where pic.id_professional = buildingo_user.id and pic.id_professional = professional.id and pic.id_project=%s", [id])
            pic = dictfetchall(cursor)
            cursor.execute("select * from image_project where id=%s", [id])
            gambar = dictfetchall(cursor)
            cursor.execute("select * from file where id=%s", [id])
            dokumen = dictfetchall(cursor)
            context = {
                'role': role,
                'row': row,
                'pic':pic,
                'gambar':gambar,
                'dokumen':dokumen,
            }
        return render(request, 'detailProject.html', context)

def detailProfessional(request, id):
    role = request.session['role']
    if role == 'Administrator':
        with connection.cursor() as cursor:
            cursor.execute("select * from professional inner join buildingo_user using(id) where id=%s", [id])
            row = dictfetchall(cursor)
            for r in row:
                profpic = "/static/profile/images/"+r['profpic']
                certificate = "/static/profile/images/"+r['certification_image']
                identity = "/static/profile/images/"+r['identity_image']
                nib = "/static/profile/file/"+r['nib_file']
            context = {
                'role': role,
                'row': row,
                'profpic':profpic,
                'certificate':certificate,
                'identity':identity,
                'nib':nib,
            }
        return render(request, 'detailProfessional.html', context)

def updateProfessional(request, id, status):
    role = request.session['role']
    if role == 'Administrator':
        admin = request.session['id']
        current_datetime = datetime.date.today()
        with connection.cursor() as cursor:
            cursor.execute("UPDATE professional SET status=%s WHERE id=%s", [status, id])
            if status == 'AVL':
                messages.success(request, "Successful Verification")
                cursor.execute("INSERT INTO professional_ver VALUES(%s, %s, %s)",[id, admin, current_datetime])
            return redirect('verifikasi:listProfessional')

def updateProject(request, id):
    role = request.session['role']
    if role == 'Administrator':
        admin = request.session['id']
        current_datetime = datetime.date.today()
        if request.method == 'POST':
            with connection.cursor() as cursor:
                cursor.execute("UPDATE project SET status='OPN' WHERE id=%s", [id])
                cursor.execute("UPDATE cancellation SET status='ACC' WHERE project_id=%s", [id])
                cursor.execute("UPDATE cancellation SET acc_date=%s WHERE project_id=%s", [current_datetime, id])
                cursor.execute("UPDATE cancellation SET admin_id=%s WHERE project_id=%s", [admin, id])
                cursor.execute("DELETE FROM PIC WHERE id_project=%s", [id])
                messages.success(request, "Project Successfully Canceled")
                return redirect('verifikasi:listProject')