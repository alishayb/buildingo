from django.urls import path
from . import views

app_name = 'verifikasi'

urlpatterns = [
    path('verif/listProfessional', views.listProfessional, name='listProfessional'),
    path('verif/listProject', views.listProject, name='listProject'),
    path('verif/detailProject/<str:id>', views.detailProject, name='detailProject'),
    path('verif/detailProfessional/<str:id>', views.detailProfessional, name='detailProfessional'),
    path('verif/updateProfessional/<str:id>/<str:status>', views.updateProfessional, name='updateProfessional'),
    path('verif/updateProject/<str:id>', views.updateProject, name='updateProject')
]