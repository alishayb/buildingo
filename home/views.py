from django.shortcuts import render
from django.contrib.sessions.models import Session

def home(request):
    context = {}
    try:
        context['role'] = request.session['role']
    except KeyError:
        request.session['role'] = ''
        context['role'] = request.session['role']

    return render(request, 'home/home.html', context)