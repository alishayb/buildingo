$(".status-item").click(function() {
  console.log("clicked")
  var idActiveStatus = $(".status-active").attr("id")
  var idActiveList = $(".list-active").attr("id")

  var idTargetStatus =$(this).attr("id")
  var idTargetList = "list-"+idTargetStatus

  var activeStatus = $("#"+idActiveStatus)
  activeStatus.removeClass("status-active")
  var targetStatus = $("#"+idTargetStatus)
  targetStatus.addClass("status-active")

  var activeList = $("#"+idActiveList)
  activeList.css("display","none")
  activeList.removeClass("list-active")

  var targetList = $("#"+idTargetList)
  targetList.css("display","block")
  targetList.addClass("list-active")
});
