from decimal import Context
from django.shortcuts import render, redirect
from .db import *

# Create your views here.

def professionals(request, type):
    role = request.session['role']
    if type == "All" :
        context = {
            'role': role,
            'professional_list': get_professional_all()
        }
    else :
        context = {
            'role': role,
            'professional_list': get_professional(type)
        }
    return render(request, 'find_professional/find_professional.html', context)

def projectrequest(request,name_id):
    if request.method == 'GET':
        professional_type = get_professional_type(name_id)
        with connection.cursor() as cursor :
            user_id = request.session['id']
            if professional_type[0][0] == "Architect" :
                cursor.execute(f"select id, title from project where owner_id = '{user_id}' and status='OPN' and type like '%Design%'")
                context = {
                    'professional_id' : name_id,
                    'professional_name' : get_professional_nama(name_id),
                    'project_list' : [{'id': row[0], 'title': row[1]} for row in cursor.fetchall()]
                }
            else :
                cursor.execute(f"select id, title from project where owner_id = '{user_id}' and status='OPN' and type like '%Construct%'")
                context = {
                    'professional_id' : name_id,
                    'professional_name' : get_professional_nama(name_id),
                    'project_list' : [{'id': row[0], 'title': row[1]} for row in cursor.fetchall()]
                }
            return render(request, 'find_professional/requestproject.html',context)

    elif request.method == 'POST':
        user_id = request.session['id']
        data = request.POST.dict()
        data.pop('csrfmiddlewaretoken')
        assign_project(data,name_id)
        # update_status_project(data, user_id)
        return redirect('project:projectList')
    

   