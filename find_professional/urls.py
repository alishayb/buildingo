from django.urls import path
from . import views

app_name = 'find_professional'

urlpatterns = [
    path('professionals/<str:type>/', views.professionals, name="professionals"),
    path('projectrequest/<str:name_id>/', views.projectrequest, name="projectrequest")
]