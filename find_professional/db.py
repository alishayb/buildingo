from django.db import connection
from collections import namedtuple

from django.http import request

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_professional_all():
    ''' Mengembalikan name, about_me, professional_type, city, province, profpic, dan gambar portfolio'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT
            P.id as id,
            B.name as name,
            B.profpic as profpic,
            P.about_me as about_me,
            P.professional_type as professional_type,
            CONCAT_WS(', ',city,province) as region
        FROM BUILDINGO.PROFESSIONAL P
        JOIN BUILDINGO.BUILDINGO_USER B ON P.id = B.id
        WHERE P.status = 'AVL';
        """)
        rows = namedtuplefetchall(cursor)
        return rows

def get_professional_id():
    ''' Mengembalikan name, about_me, professional_type, city, province, profpic, dan gambar portfolio'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT
            P.id as id
        FROM BUILDINGO.PROFESSIONAL P
        JOIN BUILDINGO.BUILDINGO_USER B ON P.id = B.id
        WHERE P.status = 'AVL';
        """)
        rows = namedtuplefetchall(cursor)
        return rows

def get_professional(type):
    ''' Mengembalikan name, about_me, professional_type, city, province, profpic, dan gambar portfolio sesuai pilihan user'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT
            P.id as id,
            B.name as name,
            B.profpic as profpic,
            P.about_me as about_me,
            P.professional_type as professional_type,
            CONCAT_WS(', ',city,province) as region
        FROM BUILDINGO.PROFESSIONAL P
        JOIN BUILDINGO.BUILDINGO_USER B ON P.id = B.id
        WHERE P.professional_type = %s and P.status = 'AVL';
        """,[type])
        rows = namedtuplefetchall(cursor)
        return rows

def get_professional_type(name):
    ''' Mengembalikan professional type dari professional'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT
            P.professional_type
        FROM BUILDINGO.PROFESSIONAL P
        JOIN BUILDINGO.BUILDINGO_USER B ON P.id = B.id
        WHERE B.id = %s;
        """,[name])
        rows = namedtuplefetchall(cursor)
        return rows

def get_professional_nama(name):
    ''' Mengembalikan nama professional dari buildingo_user'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT
            B.name
        FROM BUILDINGO.BUILDINGO_USER B 
        WHERE B.id = %s;
        """,[name])
        rows = namedtuplefetchall(cursor)
        return rows[0][0]

def assign_project(data, name_id):
    ''' menyimpan project dan professional baru di pic'''
    args = list(data.values())
    id_project = args[0]
    with connection.cursor() as cursor:
        cursor.execute("insert into BUILDINGO.PIC values(%s, %s,'REQ')",[id_project,name_id])
        return cursor

         