from django.urls import path
from . import views

app_name = 'user_profile'

urlpatterns = [
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('signup', views.signup, name='signup'),
    path('profile', views.profile, name='myProfile'),
    path('profile/edit', views.edit_profile, name='editProfile'),
    path('profile/save', views.save_profile, name='saveProfile'),
    path('profile/verify', views.verify_profile, name='verifyProfile'),
    path('profile/<str:name>', views.profile_outside, name='profileProfessional'),
]