from django import forms    
from django.core.validators import URLValidator

professional_types = [
    ('Architect', 'Architect'), 
    ('Contractor', 'Contractor')
]
structure_types = [
    ('Company','Company'), 
    ('Freelance', 'Freelance')
]
availability_status = [
    ('AVL', 'Available'),
    ('UVL', 'Unavailable')
]
provinces = [
    ('Aceh', 'Aceh'),
    ('Sumatra Utara', 'Sumatra Utara'),
    ('Sumatra Barat', 'Sumatra Barat'),
    ('Riau', 'Riau'),
    ('Kepulauan Riau', 'Kepulauan Riau'),
    ('Jambi', 'Jambi'),
    ('Bengkulu', 'Bengkulu'),
    ('Sumatra Selatan', 'Sumatra Selatan'),
    ('Kepulauan Bangka Belitung', 'Kepulauan Bangka Belitung'),
    ('Lampung', 'Lampung'),
    ('Banten', 'Banten'),
    ('Jawa Barat', 'Jawa Barat'),
    ('DKI Jakarta', 'DKI Jakarta'),
    ('Jawa Tengah', 'Jawa Tengah'),
    ('Daerah Istimewa Yogyakarta', 'Daerah Istimewa Yogyakarta'),
    ('Jawa Timur', 'Jawa Timur'),
    ('Bali', 'Bali'),
    ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
    ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
    ('Kalimantan Barat', 'Kalimantan Barat'),
    ('Kalimantan Selatan', 'Kalimantan Selatan'),
    ('Kalimantan Tengah', 'Kalimantan Tengah'),
    ('Kalimantan Timur', 'Kalimantan Timur'),
    ('Kalimantan Utara', 'Kalimantan Utara'),
    ('Gorontalo', 'Gorontalo'),
    ('Sulawesi Barat', 'Sulawesi Barat'),
    ('Sulawesi Selatan', 'Sulawesi Selatan'),
    ('Sulawesi Tengah', 'Sulawesi Tengah'),
    ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
    ('Sulawesi Utara', 'Sulawesi Utara'),
    ('Maluku', 'Maluku'),
    ('Maluku Utara', 'Maluku Utara'),
    ('Papua Barat', 'Papua Barat'),
    ('Papua', 'Papua')
]


class OptionalSchemeURLValidator(URLValidator):
    def __call__(self, value):
        if '://' not in value:
            # Validate as if it were http://
            value = 'http://' + value
        super(OptionalSchemeURLValidator, self).__call__(value)
        
class UserProfile(forms.Form):
    name = forms.CharField(widget=forms.TextInput)
    email = forms.EmailField(widget=forms.EmailInput)
    password = forms.CharField(widget=forms.PasswordInput(render_value = True))
    telephone_number = forms.IntegerField()
    profpic = forms.FileField(required=False)
    about_me = forms.CharField(widget=forms.Textarea,required=False )
    status = forms.ChoiceField(choices = availability_status)
    professional_type = forms.ChoiceField(choices = professional_types)
    types = forms.ChoiceField(choices = structure_types)
    city = forms.CharField(widget=forms.TextInput)
    province = forms.ChoiceField(choices = provinces)
    header = forms.FileField(required=False)
    website = forms.CharField(required=False, validators=[OptionalSchemeURLValidator()])
    pic = forms.CharField(widget=forms.TextInput, required=False)
    certification_name = forms.CharField(widget=forms.TextInput, required=False)
    certification_number = forms.CharField(widget=forms.TextInput)
    certification_issued_by = forms.CharField(widget=forms.TextInput, required=False)
    certification_image = forms.FileField(required=False)
    identity_image = forms.FileField(required=False)
    identity_number = forms.CharField(widget=forms.TextInput)
    nib_file = forms.FileField(required=False)

