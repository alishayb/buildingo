from django.shortcuts import redirect, render
from user_profile.forms import *
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.db import connection


def login(request):
    if request.session['id'] != '':
        return redirect('user_profile:myProfile')
    if (request.POST):
        email = request.POST['email']
        password = request.POST['password']

        with connection.cursor() as cursor:
            cursor.execute(f"SELECT id FROM BUILDINGO_USER WHERE email='{email}' and password='{password}'")
            user_id = cursor.fetchone()
            
            if(user_id is not None):
                user_id = user_id[0]
                request.session['id'] = user_id

                if user_id[0:2] == 'PO':
                    request.session['role'] = 'Project Owner'
                elif user_id[0:2] == 'PR':
                    request.session['role'] = 'Professional'
                elif user_id[0:2] == 'AD':
                    request.session['role'] = 'Administrator'
                
                return redirect('user_profile:myProfile')

            else:
                alert = {
                    'alert': 'Incorrect email or password.'
                }
                return render(request, 'profile/login.html', alert)

    return render(request, 'profile/login.html')

def logout(request):
    request.session.flush()
    request.session['id'] = ''
    request.session['role'] = ''
    return redirect('home:home')

def signup(request):
    if (request.POST):
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        telephone = request.POST['telephone']
        types = request.POST['type']
        city = request.POST['city']
        province = request.POST['province']
        professional_job = request.POST['professional-job']
        professional_type = request.POST['professional-type']

        with connection.cursor() as cursor:
            cursor.execute(f"SELECT id FROM BUILDINGO_USER WHERE id LIKE '{types}%' ")
            list_id = cursor.fetchall()

            max_id = 0
            for type_id in list_id:
                if int(type_id[0][-1]) > max_id:
                    max_id = int(type_id[0][-1])
            
            new_id = f"{types}{max_id + 1}"
            cursor.execute(f"""INSERT INTO BUILDINGO_USER VALUES (
                            '{new_id}', '{name}', '{email}', '{password}', '{telephone}', 'blank.jpg');""")
            
            if types == 'PO':
                cursor.execute(f"""INSERT INTO PROJECT_OWNER(id,city,province) VALUES (
                            '{new_id}', '{city}', '{province}');""")
                request.session['role'] = 'Project Owner'

            elif types =='PR':
                cursor.execute(f"""INSERT INTO PROFESSIONAL(id,city,province,type,professional_type) 
                                VALUES ('{new_id}', '{city}', '{province}','{professional_type}', '{professional_job}');""")
                request.session['role'] = 'Professional'

            request.session['id'] = new_id
                
            return redirect('user_profile:myProfile')

    context = {
        'provinces': provinces
    }
    return render(request, 'profile/signup.html', context)

def getUserData(user):
    return {
        'name': user[1],
        'email': user[2],
        'telephone': user[4],
        'profpic': "/static/profile/images/" + user[5]
    }

def getStatus(acronym):
    if acronym == 'UNV':
        return 'Unverified'
    elif acronym == 'VIP':
        return 'Verification in Progress'
    elif acronym == 'VER':
        return 'Verified'
    elif acronym == 'AVL':
        return 'Available'
    elif acronym == 'UVL':
        return 'Unavailable'

def profile(request):
    if request.session['id'] == '':
        return redirect('user_profile:login')
        
    role = request.session['role']
    if role != 'Project Owner' and role != 'Administrator' and role != 'Professional':
        return redirect('home:home')
    with connection.cursor() as cursor:
        user_id = request.session['id']

        cursor.execute(f"SELECT * FROM BUILDINGO_USER WHERE id='{user_id}'")
        user = list(cursor.fetchone())
        if user[5] is None:
            user[5] = 'blank.jpg'
        context = getUserData(user)

        if role == 'Project Owner':
            cursor.execute(f"SELECT * FROM PROJECT_OWNER WHERE id='{user_id}'")
            additional = cursor.fetchone()
            context['about'] = additional[1]
            context['city'] = additional[2]
            context['province'] = additional[3]

        elif role == 'Professional':
            cursor.execute(f"SELECT * FROM PROFESSIONAL WHERE id='{user_id}'")
            additional = cursor.fetchone()
            context['about'] = additional[1]
            context['status'] = getStatus(additional[2])
            context['professional_type'] = additional[3]
            context['type'] = additional[4]
            context['city'] = additional[5]
            context['province'] = additional[6]
            if additional[7] is not None:
                context['header'] = "/static/profile/images/" + additional[7]
            else:
                context['header'] = "/static/profile/images/blank.jpg"
            context['website'] = additional[8]
            context['pic'] = additional[9]
            context['certification_name'] = additional[10]
            context['certification_number'] = additional[11]
            context['certification_issued_by'] = additional[12]

            if additional[13] is not None:
                context['certification_image'] = "/static/profile/images/" + additional[13]
            else:
                context['certification_image'] = "/static/profile/images/blank.jpg"
            if additional[14] is not None:
                context['identity_image'] = "/static/profile/images/" + additional[14]
            else:
                context['identity_image'] = "/static/profile/images/blank.jpg"
            context['identity_number'] = additional[15]
            if additional[16] is not None:
                context['nib_file'] = "/static/profile/file/" + additional[16]
            else:
                context['nib_file'] = "#"
        
    context["role"] = role
    return render(request, 'profile/profile.html', context)

def profile_outside(request, name):
    with connection.cursor() as cursor:
        cursor.execute(f"SELECT * FROM BUILDINGO_USER WHERE name='{name}'")
        user = list(cursor.fetchone())
        context = getUserData(user)
        context['id'] = user[0]
        
        # Portfolio
        cursor.execute(f"SELECT id,name,city,province FROM PORTFOLIO WHERE professional_id='{user[0]}' LIMIT 3")
        portfolios = cursor.fetchall()

        list_portfolio = [] # [['Title','City','Province', 'URL Link'],...]

        for item in portfolios:
            portfolio = list(item)
            cursor.execute(f"SELECT url FROM IMAGE_PORTFOLIO WHERE portfolio_id='{item[0]}'")
            image = cursor.fetchone()
            if image is not None:
                portfolio.append(image[0])
            list_portfolio.append(portfolio)
        
        context['list_portfolio'] = list_portfolio

        # Profile
        cursor.execute(f"SELECT * FROM PROFESSIONAL WHERE id='{user[0]}'")
        additional = cursor.fetchone()
        context['about'] = additional[1]
        context['status'] = getStatus(additional[2])
        context['professional_type'] = additional[3]
        context['type'] = additional[4]
        context['city'] = additional[5]
        context['province'] = additional[6]
        if additional[7] is not None:
            context['header'] = "/static/profile/images/" + additional[7]
        else:
            context['header'] = "/static/profile/images/blank.jpg"

        context['website'] = additional[8]
        context['pic'] = additional[9]
        context['certification_name'] = additional[10]
        context['certification_number'] = additional[11]
        context['certification_issued_by'] = additional[12]

        if additional[13] is not None:
            context['certification_image'] = "/static/profile/images/" + additional[13]
        else:
            context['certification_image'] = "/static/profile/images/blank.jpg"
    
    context["role"] = request.session['role']
    return render(request, 'profile/profile_outside.html', context)


def edit_profile(request):
    if request.session['id'] == '':
        return redirect('user_profile:login')

    user_id = request.session['id']
    role = request.session['role']
    form = UserProfile()
    
    # Initial value
    with connection.cursor() as cursor:
        
        context = {
            'role': role,
        }

        cursor.execute(f"SELECT * FROM BUILDINGO_USER WHERE id='{user_id}'")
        user = cursor.fetchone()

        form.initial['name'] = user[1]
        form.initial['email'] = user[2]
        form.initial['password'] = user[3]
        form.initial['telephone_number'] = user[4]
        context["profpic"] = "/static/profile/images/" + user[5] 

        if role == 'Project Owner':
            cursor.execute(f"SELECT * FROM PROJECT_OWNER WHERE id='{user_id}'")
            additional = cursor.fetchone()
            form.initial['about_me'] = additional[1]
            form.initial['city'] = additional[2]
            form.initial['province'] = additional[3]

        elif role == 'Professional':
            cursor.execute(f"SELECT * FROM PROFESSIONAL WHERE id='{user_id}'")
            additional = cursor.fetchone()
            form.initial['about_me'] = additional[1]
            form.initial['status'] = getStatus(additional[2])
            form.fields['professional_type'].initial = additional[3]
            form.fields['types'].initial = additional[4]
            form.initial['city'] = additional[5]
            form.fields['province'].initial = additional[6]
            form.initial['website'] = additional[8]
            form.initial['pic'] = additional[9]
            form.initial['certification_name'] = additional[10]
            form.initial['certification_number'] = additional[11]
            form.initial['certification_issued_by'] = additional[12]
            form.initial['identity_number'] = additional[15]

            
            context['status'] = getStatus(additional[2])
            context["types"] = additional[4]

    context["profile_form"] = form
    return render(request, 'profile/edit_profile.html', context)

def save_profile(request):
    if request.session['id'] == '':
        return redirect('user_profile:login')
    # General Information 
    name = request.POST['name']
    email = request.POST['email']
    password = request.POST['password']
    telephone_number = request.POST['telephone_number']
    
    save_general(request, name, email, password, telephone_number)

    # Role Specific
    role = request.session['role']
    save_specific(request, role)

    return redirect('user_profile:myProfile')

def save_general(request, name, email, password, telephone_number):
    user_id = request.session['id']

    with connection.cursor() as cursor:
        cursor.execute(f"SELECT * FROM BUILDINGO_USER WHERE id='{user_id}'")
        user = cursor.fetchone()

        if 'profpic' in request.FILES:
            filename = user_id+'_profpic.jpg'
            profpic = request.FILES['profpic']
            save_file(profpic, 'image', filename)
        else:
            filename = user[5]
        
        cursor.execute(f"""UPDATE BUILDINGO_USER SET 
                        name='{name}', 
                        email='{email}', 
                        password='{password}',
                        telephone_number='{telephone_number}',
                        profpic='{filename}'
                        WHERE id='{user_id}' """)

def save_specific(request, role):
    user_id = request.session['id']
    if role == 'Project Owner':
        with connection.cursor() as cursor:
            about_me = request.POST['about_me']
            city = request.POST['city']
            province = request.POST['province']
            cursor.execute(f"""UPDATE PROJECT_OWNER SET 
                            about_me='{about_me}', 
                            city='{city}', 
                            province='{province}'
                            WHERE id='{user_id}' """)

    elif role == 'Professional':
        with connection.cursor() as cursor:
            cursor.execute(f"SELECT * FROM PROFESSIONAL WHERE id='{user_id}'")
            additional = cursor.fetchone()

            data = {
                'about_me' : request.POST['about_me'],
                'city' : request.POST['city'],
                'province' : request.POST['province'],
                'status' : request.POST.get('status', 'UNV'),
                'professional_type' : request.POST['professional_type'],
                'types' : request.POST['types'],
                'website' : request.POST['website'],
                'pic' : request.POST['pic'],
                'certification_name' : request.POST['certification_name'],
                'certification_number' : request.POST['certification_number'],
                'certification_issued_by' : request.POST['certification_issued_by'],
                'identity_number' : request.POST['identity_number']
            }

            files = {
                'header': additional[7],
                'certification_image': additional[13],
                'identity_image': additional[14],
                'nib_file': additional[16]
            }
            i = 0
            for file_key in files:
                if file_key in request.FILES:
                    file = request.FILES[file_key]
                    if file is not None:
                        if(file_key == 'nib_file'):
                            filename = user_id + '_nib.pdf'
                            save_file(file, 'file', filename)
                            files[file_key] = filename
                        else:
                            filename =  user_id + "_" + file_key + '.jpg'
                            save_file(file, 'image', filename)
                            files[file_key] = filename
                i += 1
            
            data.update(files)
            data['user_id'] = user_id
            cursor.execute("""UPDATE PROFESSIONAL SET 
                            about_me='{about_me}', 
                            city='{city}', 
                            province='{province}',
                            status='{status}',
                            professional_type='{professional_type}',
                            type='{types}',
                            website='{website}',
                            person_in_charge='{pic}',
                            certification_name='{certification_name}',
                            certification_number='{certification_number}',
                            certification_issued_by='{certification_issued_by}',
                            identity_number='{identity_number}',
                            header='{header}',
                            certification_image='{certification_image}',
                            identity_image='{identity_image}',
                            nib_file='{nib_file}'                            
                            WHERE id='{user_id}' """.format(**data))

def save_file(file, type, filename):
    if file == None:
        return
    if(type == 'image'):
        path = 'user_profile/static/profile/images/' + filename
    else:
        path = 'user_profile/static/profile/file/' + filename

    default_storage.delete(path)
    default_storage.save(path, ContentFile(file.read()))

def verify_profile(request):
    if request.session['id'] == '':
        return redirect('user_profile:login')
    role = request.session['role']
    if role != 'Project Owner' and role != 'Administrator' and role != 'Professional':
        return redirect('home:home')
    with connection.cursor() as cursor:
        user_id = request.session['id']
        cursor.execute(f"UPDATE PROFESSIONAL SET status='VIP' WHERE id='{user_id}'")

    return redirect('user_profile:myProfile')

